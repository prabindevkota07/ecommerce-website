from django.urls import path
from django.conf.urls.static import static
from . import views
from django.conf import settings
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.store, name="store"),
    path('cart/', views.cart, name="cart"),
    path('checkout/', views.checkout, name="checkout"),
    path('details/<id>', views.details, name="details"),
    path('update_item/', views.updateItem, name="update_item"),
    path('process_order/', views.processOrder, name="process_order"),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('register/', views.register, name='register'),
    path('order',views.vieworder, name='vieworder'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
